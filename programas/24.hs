import System.Random
import Data.List
import Control.Applicative
 
diff_select :: Int -> Int -> IO [Int]
diff_select n m = take n . nub . randomRs (1, m) <$> getStdGen