# EJERCICIOS HASKELL  


&nbsp;
## ***ejercicio 1***  
Encuentra el último elemento de una lista.
### **solución**  
código

    myLast :: [a] -> a
    myLast [] = error "No end for empty lists!"
    myLast [x] = x
    myLast (_:xs) = myLast xs  

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p1.png)  


&nbsp;
## ***ejercicio 2***  
Encuentra el elemento penúltimo de una lista.
### **solución**  
código

    myButLast :: [a] -> a
    myButLast = last . init  

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p2.png)  


&nbsp;
## ***ejercicio 3***  
Encuentra el elemento K'th de una lista. El primer elemento en la lista es el número 1.
### **solución**  
código

    elementAt :: [a] -> Int -> a
    elementAt list i    = list !! (i-1)

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p3.png)  


&nbsp;
## ***ejercicio 4***  
Encuentra el número de los elementos de una lista.
### solución  
**código**

    myLength           :: [a] -> Int
    myLength []        =  0
    myLength (_:xs)    =  1 + myLength xs

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p4.png)


&nbsp;
## ***ejercicio 5***  
Invierte una lista.  
### solución  
**código**

    myReverse          :: [a] -> [a]
    myReverse          =  foldl (flip (:)) []

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p5.png)


&nbsp;
## ***ejercicio 6***  
Averigüa si una lista es tras un palindrome. Un palindrome puede ser leído adelante o hacia atrás  
### solución  
**código**

    palindrom x = reverse x == x

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p6.png)


&nbsp;
## ***ejercicio 7***  
Aplanaa una estructura de lista anidada.  
Transforma una lista, posiblemente manteniendo las listas como elementos en una lista "plana" reemplazando cada lista con sus elementos (recursivamente).  
### solución  
**código**

    data NestedList a = Elem a | List [NestedList a]
    flatten :: NestedList a -> [a]
    flatten (Elem x) = [x]
    flatten (List x) = concatMap flatten x

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p7.png)


&nbsp;
## ***ejercicio 8***  
Eliminar duplicados consecutivos de elementos de lista.  
Si una lista contiene elementos repetidos, deben reemplazarse con una sola copia del elemento. El orden de los elementos no debe cambiarse.  

### solución  
**código**

    compress x = foldr (\a b -> if a == (head b) then b else a:b) [last x] x


**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p8.png)


&nbsp;
## ***ejercicio 9***  
Agrupa los duplicados consecutivos de elementos de lista en sublistas. Si una lista contiene elementos repetidos ellos deberían ser colocados en sublistas separadas.  

### solución  
**código**

    pack :: (Eq a) => [a] -> [[a]]
    pack [] = []
    pack (x:xs) = (x : takeWhile (==x) xs) : pack (dropWhile (==x) xs)


**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p9.png)


&nbsp;
## ***ejercicio 10***  

Codificación de longitud de ejecución de una lista.  
Utiliza el resultado del problema P09 para implementar el denominado método de compresión de datos de codificación de longitud de ejecución. Los duplicados consecutivos de elementos se codifican como listas (N E) donde N es el número de duplicados del elemento E.  

### solución  
**código**

    encode [] = []
    encode (x:xs) = (length $ x : takeWhile (==x) xs, x)
                 : encode (dropWhile (==x) xs)

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p10.png)


&nbsp;
## ***ejercicio 11***  

Codificación de longitud de ejecución de una lista.  
Utiliza el resultado del problema P09 para implementar el denominado método de compresión de datos de codificación de longitud de ejecución. Los duplicados consecutivos de elementos se codifican como listas (N E) donde N es el número de duplicados del elemento E.  

### solución  
en este caso el problema no queda resuelto, ya que arroja un error de variable  

**código**

    data ListItem a = Single a | Multiple Int a
    deriving (Show)
 
    encodeModified :: Eq a => [a] -> [ListItem a]
    encodeModified = map encodeHelper . encode
    where
      encodeHelper (1,x) = Single x
      encodeHelper (n,x) = Multiple n x

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p11.png)


&nbsp;
## ***ejercicio 12***  


Decodificar una lista codificada de longitud de ejecución.  
Dada una lista de códigos de longitud de ejecución generada como se especifica en el problema 11. Construya su versión sin comprimir.  

### solución  
**código**

    data ListItem a = Single a | Multiple Int a
    deriving (Show)
    decodeModified :: [ListItem a] -> [a]
    decodeModified = concatMap decodeHelper
    where
      decodeHelper (Single x)= [x]
      decodeHelper (Multiple n x) = replicate n x
      
**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p12.png)


&nbsp;
## ***ejercicio 13***  


Codificación de longitud de ejecución de una lista (solución directa).  
Implementar directamente el método de compresión de datos de codificación de longitud de ejecución. Es decir. No crear explícitamente las sublistas que contienen los duplicados, como en el problema 9, pero sólo contarlas. Como en el problema P11, simplifique la lista de resultados sustituyendo las listas de singleton (1 X) por X.  

### solución  
**código**

    data ListItem a = Single a | Multiple Int a
    deriving (Show)
    encodeDirect :: (Eq a) => [a] -> [ListItem a]
    encodeDirect [] = []
    encodeDirect (x:xs) = encodeDirect' 1 x xs
    encodeDirect' n y [] = [encodeElement n y]
    encodeDirect' n y (x:xs) | y == x    = encodeDirect' (n+1) y xs
                         | otherwise = encodeElement n y : (encodeDirect' 1 x xs)
    encodeElement 1 y = Single y
    encodeElement n y = Multiple n y

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p13.png)


&nbsp;
## ***ejercicio 14***  

Duplicar los elementos de una lista.  

### solución  
**código**

    edupli list = concat [[x,x] | x <- list]

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p14.png)


&nbsp;
## ***ejercicio 15***  


Replicar los elementos de una lista un número determinado de veces.  

### solución  
**código**

    repli :: [a] -> Int -> [a]
    repli xs n = concatMap (take n . repeat) xs

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p15.png)


&nbsp;
## ***ejercicio 16***  


Eliminar cada elemento N'th de una lista.  

### solución  
**código**

    dropEvery :: [a] -> Int -> [a]
    dropEvery [] _ = []
    dropEvery (x:xs) n = dropEvery' (x:xs) n 1 where
    dropEvery' (x:xs) n i = (if (n `divides` i) then
        [] else
        [x])
        ++ (dropEvery' xs n (i+1))
    dropEvery' [] _ _ = []
    divides x y = y `mod` x == 0

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p16.png)


&nbsp;
## ***ejercicio 17***  

Dividir una lista en dos partes; Se da la longitud de la primera parte.  
No utilice predicados predefinidos.  

### solución  
**código**

    split = flip splitAt

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p17.png)


&nbsp;
## ***ejercicio 18***  

Extraer una porción de una lista.  
Dado dos índices, i y k, la rebanada es la lista que contiene los elementos entre el i'th y k'th elemento de la lista original (ambos límites incluidos). Comienza contando los elementos con 1.  

### solución  
**código**

    eslice xs i k | i>0 = take (k-i+1) $ drop (i-1) xs

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p18.png)


&nbsp;
## ***ejercicio 19***  

Gire una lista N hacia la izquierda.  
Sugerencia: Utilice la longitud de las funciones predefinidas y (++).  

### solución  
**código**

    rotate xs n = take len . drop (n `mod` len) . cycle $ xs
    where len = length xs

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p19.png)


&nbsp;
## ***ejercicio 20***  

Quite el K'th elemento de una lista.  

### solución  
**código**

    removeAt n xs = (xs !! (n - 1), take (n - 1) xs ++ drop n xs)

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p20.png)


&nbsp;
## ***ejercicio 21***  

Inserte un elemento en una posición determinada en una lista.  

### solución  

**código**

    insertAt x xs n = take (n - 1) xs ++ [x] ++ drop (n - 1) xs

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p21.png)


&nbsp;
## ***ejercicio 22***  

Cree una lista que contenga todos los enteros dentro de un rango dado.  

### solución  
**código**

    range x y = [x..y]
      
**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p22.png)


&nbsp;
## ***ejercicio 23***  


Extraer un número dado de elementos seleccionados aleatoriamente de una lista.  

### solución  

**código**

    import System.Random (randomRIO)
    rnd_select :: [a] -> Int -> IO [a]
    rnd_select _ 0 = return []
    rnd_select (x:xs) n =
    do r <- randomRIO (0, (length xs))
       if r < n
           then do
               rest <- rnd_select xs (n-1)
               return (x : rest)
           else rnd_select xs n

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p23.png)


&nbsp;
## ***ejercicio 24***  

Sortea N números aleatorios diferentes del conjunto 1..M.  

### solución  
**código**

    import System.Random
    import Data.List
    import Control.Applicative
 
    diff_select :: Int -> Int -> IO [Int]
    diff_select n m = take n . nub . randomRs (1, m) <$> getStdGen

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p24.png)


&nbsp;
## ***ejercicio 25***  

Genere una permutación arbitraria de los elementos de una lista.  

### solución  
**código**

    import System.Random (randomRIO)
 
    rnd_permu :: [a] -> IO [a]
    rnd_permu []     = return []
    rnd_permu (x:xs) = do
    rand <- randomRIO (0, (length xs))
    rest <- rnd_permu xs
    return $ let (ys,zs) = splitAt rand rest
             in ys++(x:zs)
 
    rnd_permu' [] = return []
    rnd_permu' xs = do
    rand <- randomRIO (0, (length xs)-1)
    rest <- let (ys,(_:zs)) = splitAt rand xs
            in rnd_permu' $ ys ++ zs
    return $ (xs!!rand):rest

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p25.png)


&nbsp;
## ***ejercicio 26***  

Genere las combinaciones de K objetos distintos escogidos de los elementos de N de una lista  

### solución  
**código**

    combinations :: Int -> [a] -> [[a]]
    combinations 0 _ = [[]]
    combinations n xs = [ xs !! i : x | i <- [0..(length xs)-1], x <- combinations (n-1)       (drop (i+1) xs) ]

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p26.png)


&nbsp;
## ***ejercicio 27***  

Agrupe los elementos de un juego en subconjuntos inconexos.  

### solución  
**código**

    combination :: Int -> [a] -> [([a],[a])]
    combination 0 xs     = [([],xs)]
    combination n []     = []
    combination n (x:xs) = ts ++ ds
    where
    ts = [ (x:ys,zs) | (ys,zs) <- combination (n-1) xs ]
    ds = [ (ys,x:zs) | (ys,zs) <- combination  n    xs ]
 
    group :: [Int] -> [a] -> [[[a]]]
    group [] _ = [[]]
    group (n:ns) xs =
    [ g:gs | (g,rs) <- combination n xs
           ,  gs    <- group ns rs ]

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p27.png)

debido a que son demasiados subconjuntos, no se aprecian todos en la captura

&nbsp;
## ***ejercicio 28***  

La clasificación de una lista de listas según la longitud de sublistas   
a) suponemos esto una lista contiene los elementos que son listas ellos mismos.  
El objetivo es clasificar los elementos de esta lista según su longitud. P.ej. listas finales de selección primero, listas más largas más tarde, o viceversa.  

### solución  
**código**

    import Control.Arrow ((>>>),(&&&),second)
    import GHC.Exts (sortWith)
 
    lfsort :: [[a]] -> [[a]]
    lfsort = zip [1..] >>> map (second (length &&& id)) >>> sortWith (snd>>>fst) >>> cntDupLength undefined [] >>> sortWith (snd>>>fst) >>> sortWith fst >>> map (\(_,(_,(_,a))) -> a)
    where cntDupLength :: Int -> [(Int,(Int,a))] -> [(Int,(Int,a))] -> [(Int,(Int,(Int,a)))]
        cntDupLength _ lls [] = map ((,) (length lls)) $ reverse lls
        cntDupLength _ [] (x@(_,(l,_)):xs) = cntDupLength l [x] xs
        cntDupLength l lls ys@(x@(_,(l1,_)):xs) | l == l1   = cntDupLength l (x:lls) xs | otherwise = (map ((,) (length lls)) $ reverse lls) ++ cntDupLength undefined [] ys

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p28.png)


&nbsp;
## ***ejercicio 31***  

Determine si un número entero dado es principal.  

### solución  

**código**

    isPrime :: (Integral a) => a -> Bool
    isPrime n | n < 4 = n > 1
    isPrime n = all ((/=0).mod n) $ 2:3:[x + i | x <- [6,12..s], i <- [-1,1]]
            where s = floor $ sqrt $ fromIntegral n

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p31.png)


&nbsp;
## ***ejercicio 32***  

Determine el máximo común divisor de dos números enteros positivos. Use el algoritmo de Euclides.  

### solución  
**código**

    myGCD :: Integer -> Integer -> Integer
    myGCD a b
      | b == 0     = abs a
      | otherwise  = myGCD b (a `mod` b)
      
**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p32.png)


&nbsp;
## ***ejercicio 33***  

Determine si dos números enteros positivos son tras coprime. Dos números son coprime si su máximo común divisor iguala 1.  

### solución  

**código**

    coprime a b = gcd a b == 1

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p33.png)


&nbsp;
## ***ejercicio 34***  

Calcule la función de totient de Euler phi (m).   
La función de totient supuesta de Euler phi (m) es definida como el número de r de números enteros positiva  que es coprime a m.
Ejemplo: m = 10: r = 1,3,7,9; así phi (m) = 4. Note el caso especial: phi (1) = 1.  

### solución  
**código**

    totient 1 = 1
    totient a = length $ filter (coprime a) [1..a-1]
    where coprime a b = gcd a b == 1

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p34.png)


&nbsp;
## ***ejercicio 35***  

Determine los factores principales de un número entero dado positivo.   
Construya una lista plana que contiene los factores principales en orden ascendente.  

### solución  
**código**

    primeFactors a = let (f, f1) = factorPairOf a
                     f' = if prime f then [f] else primeFactors f
                     f1' = if prime f1 then [f1] else primeFactors f1
                 in f' ++ f1'
    where
    factorPairOf a = let f = head $ factors a
                  in (f, a `div` f)
    factors a    = filter (isFactor a) [2..a-1]
    isFactor a b = a `mod` b == 0
    prime a      = null $ factors a

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p35.png)


&nbsp;
## ***ejercicio 36***  

Determine los factores principales de un número entero dado positivo.  
Construya una lista que contiene los factores principales y su multiplicidad.  

### solución  
**código**

    import Data.List
    primeFactors :: Integer -> [Integer]
    primeFactors a = let (f, f1) = factorPairOf a
                     f' = if prime f then [f] else primeFactors f
                     f1' = if prime f1 then [f1] else primeFactors f1
                 in f' ++ f1'
    where
    factorPairOf a = let f = head $ factors a
                  in (f, a `div` f)
    factors a    = filter (isFactor a) [2..a-1]
    isFactor a b = a `mod` b == 0
    prime a      = null $ factors a
    prime_factors_mult = map encode . group . primeFactors
    where encode xs = (head xs, length xs)

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p36.png)


&nbsp;
## ***ejercicio 39***  

Una lista de números primos. Considerando una gama de números enteros por su límite más abajo y superior, construya una lista de todos los números primos en aquella gama.  

### solución  
**código**

    primesR :: Integral a => a -> a -> [a]
    primesR a b = takeWhile (<= b) $ dropWhile (< a) $ sieve [2..]
    where sieve (n:ns) = n:sieve [ m | m <- ns, m `mod` n /= 0 ]


**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p39.png)

debido a que son demasiados subconjuntos, no se aprecian todos en la captura

&nbsp;
## ***ejercicio 40***  

La conjetura de Goldbach.   
La conjetura de Goldbach dice que cada número par positivo mayor que 2 es la suma de dos números primos. Ejemplo: 28 = 5 + 23. Esto es uno de los hechos más famosos en la teoría de los números que no ha sido demostrada para ser correcta en el caso general. Numéricamente lo han confirmado hasta números muy grandes (mucho más grande que podemos ir con nuestro sistema de Prólogo).   
Escriba un predicado para encontrar los dos números primos que suman hasta un dado aún el número entero.  

### solución  
**código**

    primesR :: Integral a => a -> a -> [a]
    primesR a b = takeWhile (<= b) $ dropWhile (< a) $ sieve [2..]
    where sieve (n:ns) = n:sieve [ m | m <- ns, m `mod` n /= 0 ]
    goldbach n = head [(x,y) | x <- pr, y <- pr, x+y==n]
    where pr = primesR 2 (n-2)

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p40.png)


&nbsp;
## ***ejercicio 41***  

Considerando una gama de números enteros por su límite más abajo y superior, imprima una lista de todos los números pares y su composición Goldbach.  
En la mayoría de los casos, si un número par es escrito como la suma de dos números primos, uno de ellos es muy pequeño. Muy raras veces, el primes es ambos más grande que dicen 50. El intento de averiguar cuántos tales casos allí están en la gama 2.. 3000.

### solución  
**código**

    primesR :: Integral a => a -> a -> [a]
    primesR a b = takeWhile (<= b) $ dropWhile (< a) $ sieve [2..]
    where sieve (n:ns) = n:sieve [ m | m <- ns, m `mod` n /= 0 ]
    goldbach n = head [(x,y) | x <- pr, y <- pr, x+y==n]
    where pr = primesR 2 (n-2)
    goldbachList lb ub = map goldbach $ [even_lb,even_lb+2..ub]
    where even_lb = max ((lb+1) `div` 2 * 2) 4
    goldbachList' lb ub mv = filter (\(a,b) -> a > mv && b > mv) $
                         goldbachList lb ub

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p41.png)


&nbsp;
## ***ejercicio 46***  

Defina predicados y/2, o/2, nand/2, ni/2, xor/2, impl/2 y equ/2 (para la equivalencia lógica) que tiene éxito o el suspenso según el resultado de sus operaciones respectivas; p.ej. (y A, B) tendrá éxito, si y sólo si tanto un como la B tienen éxito.  
Una expresión lógica en dos variables entonces puede ser escrita como en el ejemplo siguiente: ((y o A, B), nand (A, B)).  
Ahora, escriba una mesa/3 de predicado que imprime la mesa de verdad de una expresión dada lógica en dos variables.  

### solución  
**código**

    and'  a b = a && b
    or'   a b = a || b
    nand' a b = not (and' a b)
    nor'  a b = not (or' a b)
    xor'  a b = not (equ' a b)
    impl' a b = or' (not a) b
    equ'  a b = a == b
    table :: (Bool -> Bool -> Bool) -> IO ()
    table f = mapM_ putStrLn [show a ++ " " ++ show b ++ " " ++ show (f a b)
                                | a <- [True, False], b <- [True, False]]



**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p46.png)


&nbsp;
## ***ejercicio 47***  

Mesas de verdad para expresiones lógicas (2).  
Siga el problema P46 por definiendo y/2, o/2, etc. como ser operadores. Esto permite escribir la expresión lógica del modo más natural, como en el ejemplo: Un (y un o no B). Defina la preferencia de operador como siempre; p. ej. como en Javanés.  

### solución  
**código**

    and'  a b = a && b
    or'   a b = a || b
    nand' a b = not (and' a b)
    nor'  a b = not (or' a b)
    xor'  a b = not (equ' a b)
    impl' a b = or' (not a) b
    equ'  a b = a == b
    table :: (Bool -> Bool -> Bool) -> IO ()
    table f = mapM_ putStrLn [show a ++ " " ++ show b ++ " " ++ show (f a b)
                                | a <- [True, False], b <- [True, False]]
    table2 :: (Bool -> Bool -> Bool) -> IO ()
    table2 f = mapM_ putStrLn [show a ++ " " ++ show b ++ " " ++ show (f a b)
                                | a <- [True, False], b <- [True, False]]
    infixl 4 `or'`
    infixl 6 `and'`
  


**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p47.png)


&nbsp;
## ***ejercicio 50***  

Códigos Huffman   
Suponemos un juego de símbolos con sus frecuencias, dadas como una lista de fr (la S, F) términos. Ejemplo: [fr (a, 45), fr (b, 13), fr (c, 12), fr (d, 16), fr (e, 9), fr (f, 5)]. Nuestro objetivo es construir una lista hc (la S, C) términos, donde C es la palabra en clave Huffman para el símbolo la S. En nuestro ejemplo, el resultado podría ser Hs = [hc (a, '0'), hc (la b, '101'), hc (c, '100'), hc (la d, '111'), hc (la e, '1101'), hc (la f, '1100')] [hc (a, '01')... etc.]. La tarea será realizada por el predicado huffman/2 definido así:

### solución  
**código**

    import Data.List
    import Data.Ord (comparing)
 
    data HTree a = Leaf a | Branch (HTree a) (HTree a)
                deriving Show
 
    huffman :: (Ord a, Ord w, Num w) => [(a,w)] -> [(a,[Char])]
    huffman freq = sortBy (comparing fst) $ serialize $
        htree $ sortBy (comparing fst) $ [(w, Leaf x) | (x,w) <- freq]
    where htree [(_, t)] = t
        htree ((w1,t1):(w2,t2):wts) =
                htree $ insertBy (comparing fst) (w1 + w2, Branch t1 t2) wts
        serialize (Branch l r) =
                [(x, '0':code) | (x, code) <- serialize l] ++
                [(x, '1':code) | (x, code) <- serialize r]
        serialize (Leaf x) = [(x, "")]

**screenshot de ejecución**  

![Sin titulo](https://gitlab.com/simmarin/haskell/raw/master/imagenes/p50.png)


&nbsp;